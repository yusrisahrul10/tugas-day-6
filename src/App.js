import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Blog } from "./page/Blog";
import { Header } from "./assets/components/Header";
import "./assets/css/global.css";
import { About } from "./page/About";
import { Memory } from "./page/Memory";

export const App = () => {
  return (
    <Router>
      <Header />
      <Switch>
        <Route exact path="/">
          <Blog />
        </Route>
        <Route exact path="/about">
          <About />
        </Route>
        <Route exact path="/memory">
          <Memory />
        </Route>
      </Switch>
    </Router>
  );
};
