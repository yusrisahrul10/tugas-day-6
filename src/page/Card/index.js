import React, { useEffect, useState } from "react";
import axios from "axios";
import { Card, CardMedia, Container, Grid, Typography } from "@mui/material";

export const CardComponent = () => {

  const api = "http://localhost:3004/postgenerated";
  const [data, setData] = useState("");

  // useEffect(() => {
  //   const fetchPokemons = async () => {
  //     try {
  //       const response = await axios.get(api);
  //       const details = await Promise.all(
  //         response.data.results.map((pokemon) => axios.get(pokemon.url))
  //       );
  //       setData(details.map((detail) => detail.data));
  //     } catch (error) {
  //       console.error("Error fetching pokemons:", error);
  //     }
  //   };

  //   fetchPokemons();
  // }, []);
    

  console.log(data);

  useEffect(() => {
    axios.get(api).then((res) => setData(res.data));
  }, []);

  return (
    <Container>
      <Typography variant="h4" component="h1" gutterBottom>
        Card Componenet
      </Typography>
      <Grid container spacing={3}>
        {(data || []).map((pokemon) => {
          return (
            <Grid item xs={12} sm={6} md={4}>
              <Card>
                <CardMedia 
                component="img"
                height="140"
                image={pokemon.img}
                alt={pokemon.lastName}/>
              </Card>
            </Grid>
          );
        })}
      </Grid>
    </Container>
  );
};
