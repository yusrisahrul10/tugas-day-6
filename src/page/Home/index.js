import React from "react";
import styled from "styled-components";
import { Homepage } from "../homepage";
import { CardComponent } from "../Card";

const Input = styled.input`
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  border-radius: 5px;
  border: 1px solid red;
  box-sizing: border-box;
`;

const Button = styled.button`
  width: ${(props) => props.width};
  border: none;
  color: ${(props) => props.color};
  outline: pointer;
  border-radius: 5px;
  padding: 10px 5px;
  margin-top: 10px;
  font-size: 40px;
  background: ${(props) => props.background};
`;

export const Home = () => {
  return (
    <div className="App">
      <h1
        style={{
          backgroundColor: "red",
          paddingTop: "20px",
        }}
      >
        Hello CodeSandbox
      </h1>
      <h2>Start editing to see some magic happen!</h2>
      <Input type="text" width="100%" height="200px" />
      <Button width="100%" color="white" background="blue">
        Click
      </Button>
      <Homepage />
      <CardComponent />
    </div>
  );
};
