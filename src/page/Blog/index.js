import { Box, Container, Grid } from "@mui/material";
import React, { useState, useEffect } from "react";
import { CardView } from "../../assets/components/Card";
import { BoxList } from "../../assets/components/List";
import axios from "axios";

export const Blog = () => {
  const api = "http://localhost:3004/postgenerated";
  const [data, setData] = useState("");

  useEffect(() => {
    axios.get(api).then((res) => setData(res.data));
  }, []);

  return (
    <Container maxWidth="lg">
      <Box sx={{ display: "flex" }}>
        <Box
          sx={{
            flex: "3 1 0",
            maxWidth: "900px",
            height: 200,
          }}
        >
          <Grid container spacing={2}>
            {(data || []).map((value) => {
              return (
                <Grid item xs={12} md={4} sm={3}>
                  <CardView path={value.img} title={value.title} subheader={value.datePost} description={value.description} />
                </Grid>
              );
            })}
          </Grid>
        </Box>
        <Box
          sx={{
            display: { xs: "none", sm: "none", md: "block" },
            flex: "1 1 0",
            height: 200,
          }}
        >
          <BoxList />
        </Box>
      </Box>
    </Container>
  );
};
