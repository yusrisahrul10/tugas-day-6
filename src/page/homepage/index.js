import React from "react";
import "./index.css";
import { styled } from "@mui/material/styles";
import { Button, Checkbox, Box,  Grid, Paper } from "@mui/material";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary
}));

export const Homepage = () => {
  return (
    <div>
      <h1>Home</h1>
      <Button variant="contained" color="secondary">
        Login
      </Button>
      <Checkbox color="error" />
      <Box sx={{ flexGrow: 1 }}>
        <Grid container spacing={2}>
          <Grid item xs={12} md={4}>
            <Item>3</Item>
          </Grid>
          <Grid item xs={12} md={4}>
            <Item>4</Item>
          </Grid>
          <Grid item xs={12} md={4}>
            <Item>5</Item>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
};
