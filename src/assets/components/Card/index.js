import { Avatar, Card, CardContent, CardHeader, CardMedia, Typography } from "@mui/material";
import { red } from "@mui/material/colors";
import React from "react";
import './style.css'

export const CardView = ({path, title, subheader, description}) => {
  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: red[500] }} aria-label="placeholder">
            R
          </Avatar>
        }
        title={title}
        subheader={subheader}
      />
      <CardMedia
        component="img"
        height="194"
        image={path}
      />
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          {description}
        </Typography>
      </CardContent>
    </Card>
  );
};